import _ from "lodash";
import model from "../mongo";

const { Product } = model;

export default {
  async products(parent) {
    return Promise.all(parent.products.map(id => Product.findById(id)));
  }
};
