import _ from "lodash";

export default {
  prices(parent) {
    return _.reverse(_.clone(parent.prices));
  }
};
