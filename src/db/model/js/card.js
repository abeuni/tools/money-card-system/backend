import model from "../mongo";

const { Transaction } = model;

export default {
  transactions(parent) {
    return Transaction.find({ card: parent.id });
  },
  async money(parent) {
    const transactions = await Transaction.find({ card: parent.id }).sort({
      datetime: 1
    });

    let amount = 0;

    transactions.forEach(transaction => {
      switch (transaction.type) {
        case "EMPTY_CARD":
          amount = 0;
          break;
        default:
          console.error(`Unknown transaction type '${transaction.type}'`);
        case "ADD_CREDITS":
        case "BUY_PRODUCT":
          amount += transaction.amount;
          break;
      }
    });

    return amount;
  }
};
