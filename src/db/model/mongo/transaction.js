import uuidv4 from "uuid/v4";
import "./db";
import { Schema, Types, model } from "mongoose";

const ObjectSchema = new Schema(
  {
    // Record
    _id: { type: Types.UUID, default: uuidv4 },

    // Data
    type: {
      type: String,
      required: true,
      enum: ["ADD_CREDITS", "BUY_PRODUCT", "EMPTY_CARD"]
    },
    card: { type: Types.UUID, ref: "Card", required: true },
    datetime: { type: Date, default: Date.now },
    amount: { type: Number },
    description: String,
    product: { type: Types.UUID, ref: "Product" }
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt"
    }
  }
);

export default model("Transaction", ObjectSchema);
