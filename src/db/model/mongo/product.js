import uuidv4 from "uuid/v4";
import "./db";
import { Schema, Types, model } from "mongoose";
import PriceChangeSchema from "./price-change-schema";
import _ from "lodash";

const ObjectSchema = new Schema(
  {
    // Record
    _id: { type: Types.UUID, default: uuidv4 },
    deletedAt: Date,

    // Data
    name: { type: String, required: true },
    prices: {
      type: [{ type: PriceChangeSchema, required: true }],
      required: true
    }
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt"
    }
  }
);

ObjectSchema.virtual("price").get(function() {
  const current = _.last(this.prices);
  return current && current.price;
});

export default model("Product", ObjectSchema);
