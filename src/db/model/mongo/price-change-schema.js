import uuidv4 from "uuid/v4";
import "./db";
import { Schema, Types } from "mongoose";

const ObjectSchema = new Schema({
  // Record
  _id: { type: Types.UUID, default: uuidv4 },

  // Data
  price: { type: Number, required: true },
  datetime: { type: Date, default: Date.now }
});

export default ObjectSchema;
