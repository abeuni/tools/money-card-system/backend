import uuidv4 from "uuid/v4";
import "./db";
import { Schema, Types, model } from "mongoose";

const ObjectSchema = new Schema(
  {
    // Record
    _id: { type: Types.UUID, default: uuidv4 },

    // Data
    number: {
      type: Number,
      required: true,
      unique: true
    }
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt"
    }
  }
);

export default model("Card", ObjectSchema);
