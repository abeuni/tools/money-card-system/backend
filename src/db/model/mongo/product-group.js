import uuidv4 from "uuid/v4";
import "./db";
import { Schema, Types, model } from "mongoose";

const ObjectSchema = new Schema(
  {
    // Record
    _id: { type: Types.UUID, default: uuidv4 },
    deletedAt: Date,

    // Data
    name: { type: String, required: true },
    products: [{ type: Types.UUID, ref: "Product", required: true }]
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt"
    }
  }
);

export default model("ProductGroup", ObjectSchema);
