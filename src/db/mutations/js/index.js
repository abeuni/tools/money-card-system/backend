import Card from "./card";
import Product from "./product";
import ProductGroup from "./product-group";

export default {
  ...Card,
  ...Product,
  ...ProductGroup
};
