import model from "../../model/mongo";
import { ApolloError } from "apollo-server-koa";
import pubsub from "../../../util/pubsub";

const { Product, PriceChangeSchema } = model;

export default {
  async productCreate(parent, args) {
    // Parse inputs
    const { name, price } = args.product;

    // Create product
    const product = await Product.create({ name, prices: [{ price }] });
    console.log(await Product.find());
    pubsub.publish("PRODUCT_LIST_UPDATED");
    return product;
  },
  async productUpdate(parent, args) {
    // Parse inputs
    const id = args.id;
    const { name, price } = args.product;

    // Find product
    const product = await Product.findById(id);
    if (!product) throw new ApolloError("Product not found!", 404);

    // Update name
    if (name) product.name = name;

    // Update price
    if (price) product.prices.push({ price });

    // Save changes
    await product.save();

    pubsub.publish("PRODUCT_UPDATED", product);

    return product;
  }
};
