import model from "../../model/mongo";
import { ApolloError } from "apollo-server-koa";
import pubsub from "../../../util/pubsub";

const { Card, Product, Transaction } = model;

export default {
  async addCredits(parent, args) {
    // Check if card exists. If not, create one
    let card = await Card.findOne({ number: args.cardNumber });
    if (!card) {
      card = await Card.create({ number: args.cardNumber });
      pubsub.publish("CARD_LIST_UPDATED");
    }

    // Create transaction
    await Transaction.create({
      type: "ADD_CREDITS",
      amount: args.amount,
      card: card.id,
      description: args.description
    });

    pubsub.publish("CARD_UPDATED", card);

    return card;
  },
  async emptyCredits(parent, args) {
    // Check if card exists. If not 404
    let card = await Card.findOne({ number: args.cardNumber });
    if (!card) throw new ApolloError("Card was not found!");

    // Create transaction
    await Transaction.create({
      type: "EMPTY_CARD",
      card: card.id,
      description: args.description
    });

    pubsub.publish("CARD_UPDATED", card);

    return card;
  },
  async buyProduct(parent, args) {
    // Check if card exists. If not 404
    const card = await Card.findOne({ number: args.cardNumber });
    if (!card) throw new ApolloError("Card was not found!", 404);

    // Checks if product exists. If not 404
    const product = await Product.findById(args.product);
    if (!product) throw new ApolloError("Product was not found!", 404);

    // Create transaction
    for (let i = 0; i < args.amount; i += 1) {
      await Transaction.create({
        type: "BUY_PRODUCT",
        card: card.id,
        amount: -product.price,
        product: args.product,
        description: args.description
      });
    }

    pubsub.publish("CARD_UPDATED", card);

    return card;
  }
};
