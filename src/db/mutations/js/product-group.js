import model from "../../model/mongo";
import { ApolloError } from "apollo-server-koa";
import pubsub from "../../../util/pubsub";

const { ProductGroup, Product } = model;

export default {
  async productGroupCreate(parent, args) {
    // Parse inputs
    const { name } = args.group;

    // Create group
    const group = await ProductGroup.create({ name });
    pubsub.publish("GROUP_LIST_UPDATED");
    return group;
  },
  async productGroupUpdate(parent, args) {
    // Parse inputs
    const id = args.id;
    const { name } = args.group;

    // Find group
    const group = await ProductGroup.findById(id);
    if (!group) throw new ApolloError("Group not found!", 404);

    // Update name
    if (name) group.name = name;

    // Save changes
    await group.save();
    console.log(group);

    pubsub.publish("GROUP_UPDATED", group);

    return group;
  },
  async productGroupAdd(parent, args) {
    // Parse inputs
    const { id, product_id } = args;

    // Find group
    const group = await ProductGroup.findById(id);
    if (!group) throw new ApolloError("Group not found!", 404);

    // Find product
    const product = await Product.findById(product_id);
    if (!product) throw new ApolloError("Product not found!", 404);

    // Update list
    group.products.addToSet(product_id);

    // Save changes
    await group.save();

    pubsub.publish("GROUP_UPDATED", group);

    return group;
  },
  async productGroupRemove(parent, args) {
    // Parse inputs
    const { id, product_id } = args;

    // Find group
    const group = await ProductGroup.findById(id);
    if (!group) throw new ApolloError("Group not found!", 404);

    // Update list
    group.products.remove(product_id);

    // Save changes
    await group.save();

    pubsub.publish("GROUP_UPDATED", group);
    pubsub.publish("GROUP_LIST_UPDATED");

    return group;
  }
};
