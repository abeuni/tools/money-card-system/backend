import Listings from "./listings";
import Directs from "./directs";

export default {
  ...Listings,
  ...Directs
};
