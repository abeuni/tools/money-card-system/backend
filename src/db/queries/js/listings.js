import { models } from "../../model";
const { Card, Product, ProductGroup } = models;

export default {
  listCards() {
    return Card.find();
  },
  listProducts() {
    return Product.find();
  },
  listProductGroups() {
    return ProductGroup.find();
  }
};
