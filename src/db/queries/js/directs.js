import { ApolloError } from "apollo-server-koa";
import { models } from "../../model";

const { Card, Product, ProductGroup } = models;

export default {
  async card(parent, args) {
    if (!(!args.number ^ !args.id))
      throw new ApolloError(
        "One, and only one of number or id is required!",
        400
      );

    let obj = null;

    if (args.id) obj = await Card.findById(args.id);
    else obj = await Card.findOne({ number: args.number });
    if (!obj) throw new ApolloError("Card not found", 404);
    return obj;
  },
  async product(parent, args) {
    const obj = await Product.findById(args.id);
    if (!obj) throw new ApolloError("Product not found", 404);
    return obj;
  },
  async productGroup(parent, args) {
    const obj = await ProductGroup.findById(args.id);
    if (!obj) throw new ApolloError("Product Group not found", 404);
    return obj;
  }
};
