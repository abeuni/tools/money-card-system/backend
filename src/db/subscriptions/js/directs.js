import { withFilter, ApolloError } from "apollo-server-koa";
import pubsub from "../../../util/pubsub";
import { models } from "../../model";

const { Card, Product, ProductGroup } = models;

const genericDirectSubscription = message => ({
  resolve: payload => payload,
  subscribe: withFilter(
    () => pubsub.asyncIterator(message),
    (payload, variables) => payload.id === variables.id
  )
});

export default {
  card: {
    resolve: payload => payload,
    subscribe: withFilter(
      () => pubsub.asyncIterator("CARD_UPDATED"),
      (payload, variables) =>
        payload.id === variables.id || payload.number === variables.number
    )
  },
  product: genericDirectSubscription("PRODUCT_UPDATED"),
  productGroup: genericDirectSubscription("GROUP_UPDATED")
};
