import directs from "./directs";
import listings from "./listings";

export default {
  ...directs,
  ...listings
};
