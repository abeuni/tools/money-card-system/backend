import { withFilter, ApolloError } from "apollo-server-koa";
import pubsub from "../../../util/pubsub";
import { models } from "../../model";

const { Card, Product, ProductGroup } = models;

export default {
  listCards: {
    subscribe: () => pubsub.asyncIterator("CARD_LIST_UPDATED")
  },
  listProducts: {
    resolve: payload => Product.find(),
    subscribe: () => pubsub.asyncIterator("PRODUCT_LIST_UPDATED")
  },
  listProductGroups: {
    subscribe: () => pubsub.asyncIterator("GROUP_LIST_UPDATED")
  }
};
